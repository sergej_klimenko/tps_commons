-module(tc_csv).
-author("Nicolas R Dufour <nicolas.dufour@nemoworld.info>").

-export([process_csv_file_with/2, process_csv_string_with/2]).
-export([process_csv_file_with/3, process_csv_string_with/3]).
-export([records_to_file/3, records_to_file/4, records_to_file/5]).
-export([rows_to_file/2, rows_to_file/3]).

%% @doc parse a csv file and process each parsed row with the RowFunction
process_csv_file_with(IoDevice, RowFunction) ->
    process_csv_file_with(IoDevice, RowFunction, []).

%% @doc parse a csv string and process each parsed row with the RowFunction
process_csv_string_with(String, RowFunction) ->
    process_csv_string_with(String, RowFunction, []).

%% @doc parse a csv file and process each parsed row with the RowFunction
%% and the initial state InitState
process_csv_file_with(IoDevice, RowFunction, RowFunctionInitState) ->
    InitState = ecsv_parser:init(RowFunction, RowFunctionInitState),
    stream_from_file(IoDevice, InitState).

%% @doc parse a csv string and process each parsed row with the RowFunction
%% and the initial state InitState
process_csv_string_with(String, RowFunction, RowFunctionInitState) ->
    InitState = ecsv_parser:init(RowFunction, RowFunctionInitState),
    stream_from_string(String, InitState).

% -----------------------------------------------------------------------------

records_to_file(File, RecModule, RecFun) ->
    records_to_file(File, RecModule, RecFun, []).

records_to_file(File, RecModule, RecFun, InitState) ->
    RecFields = RecModule:fields(),
    records_to_file(File, RecModule, RecFun, InitState, RecFields).

records_to_file(File, _RecModule, RecFun, InitState, RecFields) ->
    RecFun0 = fun(Rec) -> record_row(Rec, RecFields) end,
    RowFun = fun(State) ->
                     {RecList, NewState} = apply(RecFun, [State]),
                     {lists:map(RecFun0, RecList), NewState}
             end,
    rows_to_file(File, RowFun, InitState).

rows_to_file(File, Rows) when is_list(Rows) ->
    RowFun = fun(Counter) -> if
                                 Counter == 0 -> {Rows, length(Rows)};
                                 true -> {[], Counter}
                           end
             end,
    rows_to_file(File, RowFun); 

rows_to_file(File, RowFun) when is_function(RowFun) ->
    rows_to_file(File, RowFun, []).

rows_to_file(File, RowFun, InitState) ->
    {Rows, State} = apply(RowFun, [InitState]),
    case Rows of
        [] -> State;
        _ -> rows(File, Rows),
             rows_to_file(File, RowFun, State)
    end.

% -----------------------------------------------------------------------------

stream_from_string(String, InitState) ->
    StringIterator = fun(StringList) ->
        get_first_char(StringList)
    end,
    iterate_chars(StringIterator, String, InitState).

stream_from_file(IoDevice, InitState) ->
    IoDeviceIterator = fun(Io) ->
        {io:get_chars(Io, "", 1), Io}
    end,
    iterate_chars(IoDeviceIterator, IoDevice, InitState).

iterate_chars(IteratorFun, TextSource, State) ->
    {FirstChar, UpdatedTextSource} = IteratorFun(TextSource),

    iterate_chars(IteratorFun, UpdatedTextSource, State, FirstChar).

iterate_chars(_, _, State, eof) ->
    ecsv_parser:end_parsing(State);
iterate_chars(IteratorFun, TextSource, State, Char) ->
    UpdatedState = ecsv_parser:parse_with_character(clean_char_argument(Char), State),

    {FirstChar, UpdatedTextSource} = IteratorFun(TextSource),

    iterate_chars(IteratorFun, UpdatedTextSource, UpdatedState, FirstChar).

%% @doc make sure that an integer denoting a char is returned instead of a string
clean_char_argument([CharInt | _]) ->
    CharInt;
clean_char_argument(CharInt) when is_integer(CharInt) ->
    CharInt.

get_first_char([]) ->
    {eof, []};
get_first_char([FirstChar | Tail]) ->
    {FirstChar, Tail}.

% -----------------------------------------------------------------------------

filter_field({Field, Value}, Fields) ->
    case lists:member(Field, Fields) of
        true -> {true, Value};
        false -> false
    end.

record_row(Rec, RecFields) ->
    RowTuple = lists:zip(RecFields, tl(tuple_to_list(Rec))),
    lists:filtermap(
      fun(F) -> filter_field(F, RecFields) end,
      RowTuple).

rows(File, [Row | Rows]) ->
    row(File, Row),
    rows(File, Rows);
rows(_File, []) -> ok.

newline(File) ->
  file:write(File, "\n").

comma(File) ->
  file:write(File, ",").

field(File, Value) when is_binary(Value) ->
  Match = binary:match(Value, [<<",">>, <<"\n">>, <<"\"">>]),
  case Match of
    nomatch ->
      file:write(File, Value);
    _ ->
      file:write(File, "\""),
      file:write(File, binary:replace(Value, <<"\"">>, <<"\"\"">>)),
      file:write(File, "\"")
  end;
field(File, Value) when is_list(Value) ->
    field(File, list_to_binary(Value));
field(File, Value) when is_integer(Value) ->
    file:write(File, integer_to_list(Value));
field(File, Value) when is_float(Value) ->
    file:write(File, io_lib:format("~.2f",[Value]));
field(File, Value) when is_atom(Value) ->
    file:write(File, atom_to_list(Value));
field(File, {_, _, _} = Date) ->
    file:write(File, tc_date:format("Y-m-d", {Date, {0,0,0}})).

row(File, []) ->
  newline(File);
row(File, [Value]) ->
  field(File, Value),
  newline(File);
row(File, [Value | Rest]) ->
  field(File, Value),
  comma(File),
  row(File, Rest).
