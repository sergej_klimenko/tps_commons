-module(tc_cmd).

-export([get_args/2, get_args/3]).
-export([read_config/1]).

%%%===================================================================
%%% API functions
%%%===================================================================

read_config(E = {error, _}) ->
    E;
read_config({ok, [Config]}) ->
    {ok, prepare_config(Config)};
read_config(FileName) ->
    read_config(file:consult(FileName)).

prepare_config(List) ->
    [ prepare_config_var(K) || K <- List ].

prepare_config_var({Key, {env, EnvVar}}) ->
    case os:getenv(EnvVar) of 
        false -> throw({environment_variable_must_be_defined, EnvVar});
        VarValue -> {Key, VarValue}
    end;
prepare_config_var(V = {_, _}) -> V.

%% TL : TupleList
%% get specified params in ParamList from TL
get_args(TL, ParamList) ->
    get_args(TL, ParamList, false).

get_args(TL, ParamList, Default) ->
    try get_args0(TL, ParamList, []) of
        Params -> {ok, Params}
    catch
        error:Error ->
            case Default of 
                false ->
                    {error_params_missing, Error};
                _ ->
                    {ok, Default}
            end
    end.

%% helper func for get_router_args/2
get_args0(_TL, [], Acc) ->
    lists:reverse(Acc);
get_args0(TL, [Key|T], Acc) ->
    {Key, Val} = lists:keyfind(Key, 1, TL),
    get_args0(TL, T, [Val| Acc]).
